from flask import Flask,render_template,request
import json
import os
import webbrowser
import threading

app = Flask(__name__)


@app.route('/')
def home():
    with open(os.path.join(os.path.dirname(__file__), "spells.json")) as spells_file:
        spells = json.load(spells_file)
        sorted_spells = sorted(spells.values(), key=lambda x:x[request.args.get("sort", default="name")])
        known_spells = request.args.get("spells", default=None)
        if known_spells:
            known_spells = known_spells.split(",")
        return render_template("main.html", spells=spells, sorted_spells=sorted_spells, known_spells=known_spells)


if __name__ == '__main__':
    srv = threading.Thread(target=app.run)
    srv.start()
    webbrowser.open("http://127.0.0.1:5000")
    srv.join()