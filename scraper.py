import json
import time
import requests
from bs4 import BeautifulSoup as bs

host = "https://www.dnd-spells.com/"

spells = {}
errors = []

soup = bs(requests.get(host + "spells").text, "html.parser")
spell_pages = [x for x in soup.find(id="example").find_all("tr")][2:]

count = 0
time.clock()
for page in spell_pages:

    spell = {
        'name': [x for x in page.contents[3].stripped_strings][0],
        'level': page.contents[5].string,
        'school': page.contents[7].string,
        'cast_time': page.contents[9].string,
        'ritual': page.contents[11].string,
        'concentration': page.contents[13].string,
        # 'classes': [c.strip() for c in next(page.contents[15].stripped_strings).split('\n')],
        'source': page.contents[17].string
    }
    href = page.a['href']
    spell_soup = bs(requests.get(href).text, "html.parser").find(class_="classic-title").find_next_siblings()
    try:
        spell['range'] = spell_soup[1].contents[9].string
        spell['components'] = spell_soup[1].contents[13].string
        spell['duration'] = spell_soup[1].contents[17].string
        spell['description'] = " ".join([s for s in spell_soup[2].stripped_strings])
        if spell_soup[3].string == "At higher level":
            spell['level_bonus'] = " ".join([s for s in spell_soup[4].stripped_strings])
    except IndexError:
        errors.append(href)
    spells[spell['name']] = spell
    count += 1
    print("Added {} [{}/{}] {}".format(spell['name'], count, len(spell_pages), time.clock()))

json.dump(spells, open("spells_{}.json".format(time.time()), "w"), indent=2)
print("Error parsing these spell pages:")
print(errors)
